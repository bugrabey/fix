﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fix.App.Security
{
    public static class SecurityCacheFormats
    {
        private const string Prefix = "Security";
        public static string UserRoles(long userId)
        {
            return $"{Prefix}_User({userId})";
        }
    }
}
