﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;


namespace Fix.App.Security.Models
{
    public class TokenStoreRequest : IValidatableObject
    {
        [Required]
        [MinLength(6)]
        public string Username { get; set; }
        [Required]
        [MinLength(6)]
        public string Password { get; set; }
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return Enumerable.Empty<ValidationResult>();
            //var validator = new Validation();
            //var result = validator.Validate(this);
            //return result.Errors.Select(item => new ValidationResult(item.ErrorMessage, new[] { item.PropertyName }));
        }
    }
    //public class Validation : FluentValidation.AbstractValidator<TokenRequest>
    //{
    //    public Validation()
    //    {

    //    }
    //}

    public static class TokenModeller
    {
        public static object ToTokenStoreResponse(string token)
        {
            return new { token };
        }
    }

}
