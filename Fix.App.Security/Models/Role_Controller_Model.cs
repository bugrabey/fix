﻿using Fix.Entity.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Fix.App.Security.Models
{
    public class RoleStoreRequest
    {
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [Required]
        [MaxLength(255)]
        public string Description { get; set; }
        public bool IsActive { get; set; }
        [MinLength(1)]
        public List<string> Permissions { get; set; }
    }
    public class RoleUpdateRequest
    {
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [Required]
        [MaxLength(255)]
        public string Description { get; set; }
        public bool IsActive { get; set; }
        [MinLength(1)]
        public List<string> Permissions { get; set; }
    }

    public class RoleIndexResponse
    {

    }

    public static class RoleModeller
    {
        public static Role ToModel(this RoleStoreRequest request)
        {
            var model = new Role()
            {
                Name = request.Name,
                IsActive = request.IsActive,
                Description = request.Description,
                Permissions = request.Permissions.Select(x => new RolePermission()
                {
                    Permission = x
                }).ToList()
            };
            return model;
        }
        public static void BindTo(this RoleUpdateRequest request, Role toBind)
        {
            toBind.Name = request.Name;
            toBind.IsActive = request.IsActive;
            toBind.Description = request.Description;

            toBind.Permissions = request.Permissions.Select(x => new RolePermission()
            {
                Permission = x
            }).ToList();
        }
        public static object ToRoleIndexResponse(IEnumerable<Role> roles)
        {
            return roles.Select(x => new
            {
                roleid = x.Id,
                name = x.Name,
                isActive = x.IsActive,
                description = x.Description
            });


        }
        public static object ToRoleShowResponse(Role role)
        {
            return new
            {
                id = role.Id,
                name = role.Name,
                description = role.Description,
                role.IsActive
            };
        }

    }
}
