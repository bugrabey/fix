﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Results;

namespace Fix.App.Security.Models
{
    public class GetAuthenticationContextRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
    public class GetAuthenticationContextResponse
    {
        public string Value { get; set; }
    }

    public class GetTokenRequestValidator : AbstractValidator<GetAuthenticationContextRequest>
    {
        public GetTokenRequestValidator()
        {
            RuleFor(x => x.Username).NotNull().WithMessage("Boş geçilemez").NotEmpty().WithMessage("Boş geçilemez");
            RuleFor(x => x.Password).NotNull().WithMessage("Şifre boş olamaz").NotEmpty().MinimumLength(3).WithMessage("Şifre en az 6 karakter olmalıdır");
        }
    }
}
