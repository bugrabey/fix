﻿using Fix.Entity.Identity;
using Fix.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Fix.App.Security.Models
{
    //STORE
    public class AccountStoreRequest
    {
        [Required(AllowEmptyStrings = false)]
        [MinLength(6)]
        public string Username { get; set; }
        [Required]
        [MinLength(6)]
        public string Password { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Surname { get; set; }
        [Required]
        public DateTime CreatedOn { get; set; }
    }



    //UPDATE
    public class AccountUpdateRequest
    {
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [Required]
        [MaxLength(50)]
        public string Surname { get; set; }
        public bool IsActive { get; set; }
        public IList<long> RoleIds { get; set; }
        public DateTime ModifiedOn { get; set; }
    }

    //CHANGE
    public class AccountChangePasswordRequest
    {
        public long UserId { get; set; }
        [Required]
        [MinLength(6)]
        public string OldPassword { get; set; }

        [Required]
        [MinLength(6)]
        public string NewPassword { get; set; }
    }


    //MODELLER
    public static class AccountModeller
    {
        public static object ToAccountIndexResponse(IPagedList<User> users)
        {
            var items = users.Select(x => new
            {
                userid = x.Id,
                name = x.Name,
                surname = x.Surname,
                username = x.Username,
                email = x.Email,
                mobilePhone = x.MobilePhone,
                createdOn = x.CreatedOn,
                modifiedOn = x.ModifiedOn,
                isActive = x.UserLoginStatus == UserLoginStatus.Active,
            });

            return new
            {
                Data = items,
                Paging = new
                {
                    TotalCount = users.TotalCount,
                    TotalPages = users.TotalPages,
                    PageSize = users.PageSize,
                    PageIndex = users.PageIndex
                }
            };
        }
        public static User ToModel(this AccountStoreRequest request)
        {
            var model = new User();
            model.Name = request.Name;
            model.Surname = request.Surname;
            model.Username = request.Username;
            model.Password = request.Password;
            return model;
        }
        public static User ToModel(this AccountUpdateRequest request, User instance)
        {
            instance.Name = request.Name;
            instance.Surname = request.Surname;
            instance.ModifiedOn = request.ModifiedOn;
            var toRemove = instance.UserInRoles.Select(x => x.Id).Except(request.RoleIds).ToList();
            var toAdd = request.RoleIds.Except(instance.UserInRoles.Select(x => x.Id)).ToList();

            foreach (var id in toRemove)
            {
                var item = instance.UserInRoles.Where(x => x.Id == id).FirstOrDefault();
                instance.UserInRoles.Remove(item);
            }

            foreach (var id in toAdd)
            {
                instance.UserInRoles.Add(new UserInRole() { Id = id });
            }
            return instance;
        }
        public static object ToUserShowResponse(User user)
        {
            return new
            {
                User = new
                {
                    UserId = user.Id,
                    username = user.Username,
                    name = user.Name,
                    surname = user.Surname,
                    email = user.Email,
                    mobilePhone = user.MobilePhone,
                    createdBy = user.CreatedBy,
                    modifiedBy = user.ModifiedBy,
                    modifiedOn = user.ModifiedOn,
                    createdOn = user.CreatedOn
                },
                Roles = user.UserInRoles.Select(x => new
                {
                    x.Role.Id,
                    x.Role.Name,
                    x.Role.Description,
                    x.Role.IsActive
                })
            };
        }

    }
}
