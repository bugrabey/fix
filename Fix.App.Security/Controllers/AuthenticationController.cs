﻿using System;
using System.Threading.Tasks;
using Fix.Domain.Security;
using Fix.Domain.Security.Models;
using Fix.Logging;
using Fix.Logging.Loggers.Elasticsearch;
using Fix.Security.OpenAuthentication.Jwt;
using Fix.App.Security.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Fix.Exceptions;
using Microsoft.AspNetCore.Cors;
using Fix.Domain.Security.Services;

namespace Fix.App.Security.Controllers
{
    [Route("api/[controller]")]
    public class AuthenticationController : ControllerBase
    {
        private readonly ILogManager logManager;
        private readonly IAccountService accountService;
        private readonly IWorkContext workContext;
        private readonly IExceptionManager exceptionManager;

        public AuthenticationController(ILogManager logManager, IAccountService accountService, IWorkContext workContext, IExceptionManager exceptionManager)
        {
            this.logManager = logManager ?? throw new ArgumentNullException(nameof(logManager));
            this.accountService = accountService ?? throw new ArgumentNullException(nameof(accountService));
            this.workContext = workContext ?? throw new ArgumentNullException(nameof(workContext));
            this.exceptionManager = exceptionManager ?? throw new ArgumentNullException(nameof(exceptionManager));
        }


        [HttpPost("token")]
        [AllowAnonymous]
        [DisableCors]
        public async Task<IActionResult> Token([FromBody]GetAuthenticationContextRequest request)
        {
            var context = await accountService.HandleAuthenticationAsync(request.Username, request.Password);
            if (!string.IsNullOrEmpty(context))
            {
                return Ok(context);
            }
            return Unauthorized();
        }
    }
}

