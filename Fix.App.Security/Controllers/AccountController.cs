﻿using System;
using Fix.Caching;
using Fix.App.Security.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Fix.Mvc.Filters;
using Fix.Domain.Security;
using Fix.Mvc;
using Fix.Logging;
using System.Threading.Tasks;

namespace Fix.App.Security.Controllers
{
    [EnableCors("AllowAll")]
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private readonly ISecurityDomain securityDomain;
        private readonly ICacheManager cacheManager;
        private readonly ILogManager logManager;

        public AccountController(
            ISecurityDomain securityDomain,
            ICacheManager cacheManager,
            ILogManager logManager
            )
        {
            this.securityDomain = securityDomain ?? throw new ArgumentNullException(nameof(securityDomain));
            this.cacheManager = cacheManager ?? throw new ArgumentNullException(nameof(cacheManager));
            this.logManager = logManager ?? throw new ArgumentNullException(nameof(logManager));
        }


        [HttpGet("page/{pageIndex}")]
        [FixAuthorization(Permission = AccountPermissions.VIEW_USERS)]
        public async Task<IActionResult> Index(int pageIndex)
        {
            var users = await securityDomain.Account.GetUsersAsync(pageIndex);
            var response = AccountModeller.ToAccountIndexResponse(users);
            return Ok(response);
        }


        [HttpPost]
        [FixAuthorization(Permission = AccountPermissions.REGISTER_USER)]
        public async Task<IActionResult> Store([FromBody]AccountStoreRequest request)
        {
            var user = request.ToModel();
            await securityDomain.Account.RegisterAsync(user);
            return Ok();
        }

        [HttpPut("{id}")]
        [FixAuthorization(Permission = AccountPermissions.MANAGE_USER)]
        public async Task<IActionResult> Update(int id, [FromBody] AccountUpdateRequest request)
        {
            var user = await securityDomain.Account.GetUserWithRelationsAsync(id);
            request.ToModel(user);
            await securityDomain.Account.SaveUserAsync(user);
            return Ok();
        }

        [HttpGet("activate/{userId}")]
        [FixAuthorization(Permission = AccountPermissions.ACTIVATE_USER)]
        public async Task<IActionResult> Activate(long userId)
        {
            await securityDomain.Account.ActivateAsync(userId);
            return Ok();
        }


        [HttpGet("show/{id}")]
        [FixAuthorization(Permission = AccountPermissions.VIEW_USER)]
        public async Task<IActionResult> Show(int id)
        {
            var user = await securityDomain.Account.GetUserWithRelationsAsync(id);
            var response = AccountModeller.ToUserShowResponse(user);
            await logManager.Logger.InfoAsync("User showed");
            return Ok(response);
        }


        [HttpPost("changepassword")]
        [FixAuthorization(Permission = AccountPermissions.CHANGE_PASSWORD)]
        public async Task<IActionResult> ChangePassword([FromBody]AccountChangePasswordRequest request)
        {
             await securityDomain.Account.ChangePasswordAsync(request.UserId, request.OldPassword, request.NewPassword);
            return Ok();
        }
    }
}


