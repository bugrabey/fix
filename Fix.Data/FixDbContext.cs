﻿using Microsoft.EntityFrameworkCore;
using System;

namespace Fix.Data
{
    public class FixDbContext : DbContext
    {
        private readonly IEntityConfiguratorProvider configuratorProvider;

        public FixDbContext(DbContextOptions<FixDbContext> options, IEntityConfiguratorProvider configuratorProvider) : base(options)
        {
            this.configuratorProvider = configuratorProvider ?? throw new ArgumentNullException(nameof(configuratorProvider));
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var configurators = configuratorProvider.GetConfigurators<FixDbContext>();

            foreach (var configurator in configurators)
            {
                configurator.Configure(modelBuilder);
            }
        }
    }
}
