﻿using Fix.Entity;
using Fix.Entity.Utility;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Linq;

namespace Fix.Data.Extensions
{
    public static class ModelBuilderExtensions
    {
        public static ModelBuilder TraceableEntity<TEntity>(this ModelBuilder builder, Action<EntityTypeBuilder<TEntity>> buildAction) where TEntity : class, ITraceable, IOptimistic
        {
            var modelBuilder = builder.Entity(buildAction);

            return modelBuilder.Entity<TEntity>(x =>
            {
                x.Property(p => p.RowVersion).IsRowVersion().IsConcurrencyToken();
            });
        }
        public static ModelBuilder LookupEntity<TEntity>(this ModelBuilder builder) where TEntity : LookupTable
        {
            return LookupEntity<TEntity>(builder, null);
        }
        public static ModelBuilder LookupEntity<TEntity>(this ModelBuilder builder, Action<EntityTypeBuilder<TEntity>> buildAction) where TEntity : LookupTable
        {
            if (buildAction != null)
                builder = builder.Entity(buildAction);

            return builder.Entity<TEntity>(x =>
            {
                x.HasKey(p => p.Id);
                x.Property(p => p.Id).ValueGeneratedNever();
                x.HasIndex(p => p.Name).IsUnique();
                x.Property(p => p.Description).HasMaxLength(250);

                var items = LookupTable.GetAll<TEntity>().ToArray();
                x.HasData(items);
            });
        }
    }
}
