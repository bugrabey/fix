﻿using Fix.Data.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Data.Extensions
{
    public static class DatabaseExtensions
    {
        public static DatabaseConfig GetDatabaseConfig(this IConfiguration configuration, string name)
        {
            var section = configuration.GetSection(name);
            var config = new DatabaseConfig();
            section.Bind(config);

            return config;
        }

        public static IServiceCollection UseEntityFramework<T>(this IServiceCollection services, DatabaseConfig configuration) where T : DbContext
        {
            if (configuration.Provider == "PostgreSql")
                return services.AddEntityFrameworkNpgsql().AddDbContext<FixDbContext>(options =>
                {
                    options.UseNpgsql(configuration.ConnectionString);
                    // options.UseApplicationServiceProvider(services)
                });

            if (configuration.Provider == "MSSql" || configuration.Provider == "SqlExpress")
                return services.AddDbContext<FixDbContext>(options => options.UseSqlServer(configuration.ConnectionString));

            throw new Exception("InvalidCastException provider name");
        }
    }

}
