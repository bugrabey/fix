﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Fix.Data
{
    public class TransactionStrategyManager : ITransactionStrategyManager
    {
        private readonly ITransactionStrategyFactory handlerFactory;

        public TransactionStrategyManager(ITransactionStrategyFactory handlerFactory)
        {
            this.handlerFactory = handlerFactory ?? throw new ArgumentNullException(nameof(handlerFactory));
        }

        public void Apply(IEnumerable<EntityEntry> entries)
        {
            var strategies = handlerFactory.Create();
            foreach (var entry in entries)
            {
                if (strategies.Count() > 0)
                {
                    foreach (var strategy in strategies.OrderBy(x => x.Order))
                    {
                        strategy.Apply(entry);
                    }
                }
            }
        }
    }
}
