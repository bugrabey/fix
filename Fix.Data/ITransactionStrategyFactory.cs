﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fix.Data
{
    public interface ITransactionStrategyFactory : IDependency
    {
        IEnumerable<ITransactionStrategy> Create();
    }
    public class TransactionStrategyFactory : ITransactionStrategyFactory
    {
        private readonly IEnumerable<ITransactionStrategy> strategies;

        public TransactionStrategyFactory(IEnumerable<ITransactionStrategy> strategies)
        {
            this.strategies = strategies ?? throw new ArgumentNullException(nameof(strategies));
        }
        public IEnumerable<ITransactionStrategy> Create()
        {
            return strategies;
        }
    }
}
