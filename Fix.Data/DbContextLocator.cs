﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Fix.Data
{
    public class DbContextLocator : IDbContextLocator
    {
        private readonly FixDbContext dbContext;
        public DbContextLocator(FixDbContext dbContext) => this.dbContext = dbContext;
        DbContext IDbContextLocator.Current => dbContext;
    }
}
