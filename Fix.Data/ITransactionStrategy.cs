﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Data
{
    public interface ITransactionStrategy : IScoped
    {
        void Apply(EntityEntry entry);
        int Order { get; }
    }
}
