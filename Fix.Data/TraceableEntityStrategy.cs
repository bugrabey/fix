﻿using System;
using System.Collections.Generic;
using System.Text;
using Fix.Clock;
using Fix.Entity;
using Fix.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Fix.Data
{
    public class TraceableEntityStrategy : ITransactionStrategy
    {
        private readonly IWorkContext workContext;
        private readonly IClock clock;

        public TraceableEntityStrategy(IWorkContext workContext, IClock clock)
        {
            this.workContext = workContext ?? throw new ArgumentNullException(nameof(workContext));
            this.clock = clock ?? throw new ArgumentNullException(nameof(clock));
        }
        public int Order => 1;

        public void Apply(EntityEntry entry)
        {
            if (typeof(ITraceable).IsAssignableFrom(entry.Entity.GetType()))
            {
                var entity = entry.Entity as ITraceable;
                switch (entry.State)
                {
                    case EntityState.Detached:
                    case EntityState.Unchanged:
                    case EntityState.Deleted:
                        break;
                    case EntityState.Modified:
                        SetValuesForUpdate(entity);
                        break;
                    case EntityState.Added:
                        SetValueForInsert(entity);
                        break;
                }
            }
        }
        private void SetValueForInsert(ITraceable entity)
        {
            entity.CreatedOn = clock.Timestamp();
            entity.CreatedBy = GetUserId();
        }
        private void SetValuesForUpdate(ITraceable entity)
        {

            entity.ModifiedOn = clock.Timestamp();
            entity.ModifiedBy = GetUserId();
        }

        private long GetUserId()
        {
            return (workContext.AuthenticationService.IsAuthenticated ? workContext.AuthenticationService.GetContext<ClientContext>().UserId : 0);
        }
    }
}
