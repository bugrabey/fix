﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Data.Configuration
{
    public class DatabaseConfig
    {
        public string Provider { get; set; }
        public string ConnectionString { get; set; }
    }
}
