﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Runtime;

namespace Fix.SandBox
{
    class Program
    {
        static void Main(string[] args)
        {
         
         
            ServicePointManager.UseNagleAlgorithm = true;
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.CheckCertificateRevocationList = true;
            ServicePointManager.DefaultConnectionLimit = 1000;

            Console.WriteLine("Start");
            object locker = new object();

            for (int i = 0; i < 10; i++)
            {
                var token = HttpServices.GetCustomer();
                Console.WriteLine(token);
                Parallel.For(0, 100, (x) =>
                {
                    HttpServices.GetCustomer();
                    //   Console.WriteLine(token);
                    //var response = HttpServices.GetUsers(token);
                    //Console.WriteLine(DateTime.Now + " -> " + response);
                });
            }
            Console.WriteLine("Hello World!");
            Console.Read();

        }

        private static string RefreshToken()
        {
            return HttpServices.GetToken().Replace("\"", "");

        }
    }
}
