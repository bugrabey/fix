﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Fix.SandBox
{
    public static class HttpServices
    {
        private static string Url = "http://localhost:33171/api/authentication/token";
        private static RestClient client = new RestClient();
        public static string GetToken()
        {
            client.BaseUrl = new Uri(Url);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Postman-Token", "84567a20-2d08-486c-8021-2b12e7c246f0");
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("undefined", "{\r\n  \"username\": \"Administrator0\",\r\n\t\"password\": \"Administrator\"\r\n}\r\n\r\n", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public static string GetCustomer()
        {
            client.BaseUrl = new Uri("http://localhost:33171/api/bugra/customers");
            var request = new RestRequest(Method.GET);
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("undefined", "{\r\n  \"username\": \"Administrator0\",\r\n\t\"password\": \"Administrator\"\r\n}\r\n\r\n", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public static string GetUsers(string token)
        {
            //var client = new RestClient("http://localhost:33171/api/account/show/20");
            client.BaseUrl = new Uri("http://localhost:33171/api/account/show/20");
            var request = new RestRequest(Method.GET);
            request.AddHeader("Postman-Token", "11668b42-369b-4f48-97d6-7275a0481b5b");
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("Authorization", "Bearer " + token);
            var response = client.Execute(request);
            return response.StatusCode.ToString();
        }

        public static async Task<Response<TPull>> PostAsync<TPull>(object body, string url) where TPull : class
        {
            client.BaseUrl = new Uri(url);
            var restRequest = new RestRequest(Method.POST);
            restRequest.AddJsonBody(body);

            return await CallAsync<TPull>(restRequest);
        }

        public static async Task<Response<TPayload>> CallAsync<TPayload>(IRestRequest request) where TPayload : class
        {
            var response = await client.ExecuteTaskAsync<Response<TPayload>>(request);
            return await InitResponseAsync<TPayload>(response);
        }

        public static async Task<Response<TPayload>> GetAsync<TPayload>(string url) where TPayload : class
        {
            client.BaseUrl = new Uri(url);
            var request = new RestRequest(Method.GET);

            return await CallAsync<TPayload>(request);
        }
        private static async Task<Response<TPayload>> InitResponseAsync<TPayload>(IRestResponse response) where TPayload : class
        {
            if (response.IsSuccessful)
            {
                return new SuccessResponse<TPayload>
                {
                    Payload = JsonConvert.DeserializeObject<TPayload>(response.Content)
                };
            }
            else
            {
                return await CreateFailedResponseAsync<TPayload>(response.Content);
            }
        }


        public static async Task<Response<TPayload>> CreateFailedResponseAsync<TPayload>(string content) where TPayload : class
        {
            return await Task.Run(() =>
            {
                return new FailedResponse<TPayload> { Payload = JsonConvert.DeserializeObject<TPayload>(content) };
            });
        }
    }
}
