﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.SandBox
{
    public class BaseRequest
    {
    }
    public abstract class Response<TPayload> where TPayload : class
    {
        public TPayload Payload { get; set; }
        public abstract bool IsSuccess { get; }
    }
    public class SuccessResponse<TPayload> : Response<TPayload> where TPayload : class
    {
        public override bool IsSuccess { get => true; }
    }
    public class FailedResponse<TPayload> : Response<TPayload> where TPayload : class
    {
        public override bool IsSuccess { get => false; }
    }

    public class FailedBody
    {
        public string Message { get; set; }
    }

}
