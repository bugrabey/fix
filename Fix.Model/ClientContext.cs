﻿using Fix.Security.OpenAuthentication;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Model
{
    public class ClientContext : IClientContext
    {
        public string Key { get; set; }
        public IEnumerable<string> Permissions { get; set; }
        public long UserId { get; set; }
    }
}
