﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.App.Customer.Controllers
{
    [EnableCors("AllowAll")]
    [Route("api/[controller]")]
    public class bugraController : Controller
    {
        [HttpGet("customers")]
        public IActionResult GetCustomers()
        {
            return Ok("BUGRA");
        }
    }
}
