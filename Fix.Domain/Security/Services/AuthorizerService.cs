﻿using Fix.Caching;
using Fix.Data;
using Fix.Domain.Security.Models;
using Fix.Environment;
using Fix.Model;
using Fix.Security;
using Fix.Security.OpenAuthentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fix.Domain.Security.Services
{
    public class AuthorizerService : IAuthorizer
    {
        private readonly IAuthenticationService authenticationService;

        public AuthorizerService(IAuthenticationService authenticationService)
        {
            this.authenticationService = authenticationService ?? throw new ArgumentNullException(nameof(authenticationService));
            
        }
        public bool Authorize(string permission)
        {
            if (string.IsNullOrEmpty(permission))
                throw new ArgumentNullException(nameof(permission));

            var authorize = false;

            var client = authenticationService.GetContext<ClientContext>();
            if (client != null)
                authorize = client.Permissions.Contains(permission);

            return authorize;
        }

    }
}
