﻿using Fix.Entity.Identity;
using Fix.Model;
using Fix.Utility;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Fix.Domain.Security.Services
{

    public interface IAccountService : IScoped
    {
        Task<bool> IsUserExistAsync(string username);
        Task SaveUserAsync(User user);
        Task CreateUserAsync(User user);
        Task<User> GetUserAsync(long userId);
        Task<User> GetUserWithRelationsAsync(long userId);
        Task<User> GetUserAsync(string username);
        Task<User> GetUserAsync(string username, string password);
        Task<bool> CanAuthenticateAsync(string username, string password);
        Task<PagedList<User>> GetUsersAsync(int pageIndex);
        Task RegisterAsync(User user);
        Task ActivateAsync(long userId);
        Task AddActivityAsync(UserActivityLog activityLog);
        Task AddRoleAsync(Role role);
        Task SaveRoleAsync(Role role);
        Task<Role> GetRoleAsync(int id);
        Task<Role> GetRoleWithRelationsAsync(int id);
        Task<IEnumerable<Role>> GetRolesAsync();
        Task<bool> IsRoleExistAsync(string name);
        Task<IEnumerable<string>> GetPermissionsAsync();
        Task<IEnumerable<string>> UserPermissionsAsync(string userName);
        Task ChangePasswordAsync(long userId, string oldPassword, string newPassword);
        Task<ClientContext> CreateUserContextAsync(User user);
        Task<string> HandleAuthenticationAsync(string username, string password);

        Task<IEnumerable<UserLoginStatus>> GetUserStatus();
    }
}
