﻿using Fix.Caching;
using Fix.Data;
using Fix.Entity.Identity;
using Fix.Exceptions.Identity;
using Fix.Logging;
using Fix.Model;
using Fix.Security;
using Fix.Security.Cryptography;
using Fix.Security.OpenAuthentication.Jwt;
using Fix.Utility;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fix.Domain.Security.Services
{
    public class AccountService : IAccountService
    {
        private readonly IAuthenticationService authenticationService;
        private readonly IWorkContext workContext;
        private readonly ILogManager logManager;
        private readonly ICryptoService cryptoService;
        private readonly IRepository<User> userRepository;
        private readonly IRepository<Role> roleRepository;
        private readonly IRepository<UserLoginStatus> userstatusRe;
        private readonly IRepository<RolePermission> rolePermissionRepository;
        private readonly IRepository<UserInRole> userInRoleRepository;
        private readonly IEnumerable<IPermissionProvider> permissionProviders;
        private readonly ICacheManager cacheManager;
        public AccountService(
            ICryptoService cryptoService,
            IAuthenticationService authenticationService,
            IWorkContext workContext,
            ILogManager logManager,
            IRepository<User> userRepository,
            IRepository<UserInRole> userInRoleRepository,
            IRepository<Role> roleRepository,
            IRepository<UserLoginStatus> UserstatusRe,
            IRepository<RolePermission> rolePermissionRepository,
            IEnumerable<IPermissionProvider> permissionProviders,
            ICacheManager cacheManager
            )
        {
            this.cryptoService = cryptoService ?? throw new ArgumentNullException(nameof(cryptoService));
            this.authenticationService = authenticationService ?? throw new ArgumentNullException(nameof(authenticationService));
            this.workContext = workContext ?? throw new ArgumentNullException(nameof(workContext));
            this.logManager = logManager ?? throw new ArgumentNullException(nameof(logManager));
            this.userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            this.roleRepository = roleRepository ?? throw new ArgumentNullException(nameof(roleRepository));
            userstatusRe = UserstatusRe ?? throw new ArgumentNullException(nameof(UserstatusRe));
            this.rolePermissionRepository = rolePermissionRepository ?? throw new ArgumentNullException(nameof(rolePermissionRepository));
            this.userInRoleRepository = userInRoleRepository ?? throw new ArgumentNullException(nameof(userInRoleRepository));
            this.permissionProviders = permissionProviders ?? throw new ArgumentNullException(nameof(permissionProviders));
            this.cacheManager = cacheManager ?? throw new ArgumentNullException(nameof(cacheManager));
        }

        public async Task<bool> IsUserExistAsync(string username)
        {
            return await userRepository.AnyAsync(x => x.Username == username);
        }

        public async Task<bool> CanAuthenticateAsync(string username, string password)
        {

            var user = await GetUserAsync(username, password);
            return (user.UserLoginStatus == UserLoginStatus.Active);

            //password = cryptoService.Encrypt(password, HashTypes.MD5).Value;
            //bool result = await userRepository.AnyAsync(u => u.Username == username && u.Password == password && u.UserLoginStatus == UserLoginStatus.Active);
            //return result;
        }

        public async Task<User> GetUserAsync(string username)
        {
            var user = await userRepository.FirstOrDefaultAsync(x => x.Username == username);
            if (user == null)
                throw new UserNotFoundException("The specified username(" + username + ") not found.");

            return user;
        }
        public async Task<User> GetUserAsync(string username, string password)
        {
            var user = await GetUserWithRelationsAsync(username);
            if (user.Password != cryptoService.Encrypt(password, HashTypes.MD5).Value)
                throw new PasswordMismatchException("wrong password");

            return user;
        }

        public async Task RegisterAsync(User user)
        {
            if (await IsUserExistAsync(user.Username))
            {
                throw new UserAlreadyExistsException("The user already exists!");
            }

            user.UserLoginStatus = UserLoginStatus.NewRegistered;
            user.Password = cryptoService.Encrypt(user.Password, HashTypes.MD5).Value;
            await userRepository.AddAsync(user);
        }

        public async Task ActivateAsync(long userId)
        {
            var user = await GetUserAsync(userId);
            if (user == null)
                throw new UserNotFoundException("The specified user id(" + userId + ") not found");

            userRepository.Update(user);
        }

        public async Task<User> GetUserAsync(long userId)
        {
            return await userRepository.FirstOrDefaultAsync(x => x.Id == userId);
        }


        public async Task<User> GetUserWithRelationsAsync(long userId)
        {
            var user = await userRepository.Table
                .Include(l => l.Logs)
                .Include(u => u.UserInRoles)
                .ThenInclude(r => r.Role)
                .FirstOrDefaultAsync(x => x.Id == userId);


            if (user == null)
                throw new UserNotFoundException("The specified user Id(" + userId + ") not found");

            return user;
        }
        public async Task<User> GetUserWithRelationsAsync(string username)
        {
            var user = await userRepository.Table
                .Include(l => l.Logs)
                .Include(u => u.UserInRoles)
                .ThenInclude(r => r.Role)
                .ThenInclude(r => r.Permissions)

                .FirstOrDefaultAsync(x => x.Username == username);
            if (user == null)
                throw new UserNotFoundException("The specified user Id(" + username + ") not found");

            return user;
        }

        public async Task CreateUserAsync(User user)
        {
            user.Password = cryptoService.Encrypt(user.Password, HashTypes.MD5).Value;
            await userRepository.AddAsync(user);
        }
        public async Task SaveUserAsync(User user)
        {
            await userRepository.UpdateAsync(user);
        }

        public async Task AddActivityAsync(UserActivityLog activityLog)
        {
             //await userRepository.AddAsync(activityLog);
        }


        public async Task<IEnumerable<string>> GetPermissionsAsync()
        {
            return permissionProviders.SelectMany(x => x.GetPermissions());
        }

        public async Task<IEnumerable<string>> UserPermissionsAsync(string username)
        {
            var items = await GetUserWithRelationsAsync(username);
            return items.UserInRoles.SelectMany(x => x.Role.Permissions.Select(p => p.Permission));
        }

        public async Task<bool> IsRoleExistAsync(string name)
        {
            return await roleRepository.AnyAsync(x => x.Name == name);
        }
        public async Task<bool> IsRoleExistAsync(int id)
        {
            return await roleRepository.AnyAsync(x => x.Id == id);
        }
        public async Task AddRoleAsync(Role role)
        {
            if (await IsRoleExistAsync(role.Name))
            {
                throw new RoleExistsException("Role exist");
            }

            var permissions = await GetPermissionsAsync();
            if (role.Permissions.Select(x => x.Permission).Except(permissions).Any())
            {
                throw new UnKnowPermissionException("One or more permission is undefined");
            }
            roleRepository.Add(role);
        }
        public async Task SaveRoleAsync(Role role)
        {
            if (await IsRoleExistAsync(role.Name))
            {
                throw new RoleExistsException("Role exist.");
            }

            var availables = await GetPermissionsAsync();
            if (role.Permissions.Select(x => x.Permission).Except(availables).Any())
            {
                throw new UnKnowPermissionException("One or more permission is undefined");
            }
            await roleRepository.UpdateAsync(role);
        }
        public async Task<Role> GetRoleAsync(int id)
        {
            var role = await roleRepository.FirstOrDefaultAsync(x => x.Id == id);

            if (role == null)
                throw new RoleNotFoundException("The specified role id{" + id + "} not found.");

            return role;
        }
        public Task<Role> GetRoleWithRelationsAsync(int id)
        {
            var role = roleRepository.Table.Include(x => x.Permissions).FirstOrDefaultAsync(x => x.Id == id);

            if (role == null)
                throw new RoleNotFoundException($"The specified role id({id}) not found.");

            return role;
        }
        public async Task<IEnumerable<Role>> GetRolesAsync()
        {
            return await roleRepository.Table.ToListAsync();
        }

        public async Task<bool> AuthorizeAsync(string permission)
        {
            var user = authenticationService.GetContext<ClientContext>();

            if (user == null)
                throw new Exception("ClientContext not found!");

            var permissions = await UserPermissionsAsync(user.Key);
            return permissions.Contains(permission);
        }

        public async Task ChangePasswordAsync(long userId, string oldPassword, string NewPassword)
        {
            var user = await GetUserAsync(userId);
            if (user.UserLoginStatus != UserLoginStatus.Active)
                throw new UserDeactiveException("User state is not active.");

            if (user.Password != cryptoService.Encrypt(oldPassword, HashTypes.MD5).Value)
            {
                throw new PasswordMismatchException("Passwords are mismatch!");
            }
            user.Password = cryptoService.Encrypt(NewPassword, HashTypes.MD5).Value;
            await userRepository.UpdateAsync(user);
        }

        public async Task<ClientContext> CreateUserContextAsync(User user)
        {
            var permissions = await UserPermissionsAsync(user.Username);
            return new ClientContext { Key = user.Username, Permissions = permissions.ToList(), UserId = user.Id };
        }


        public async Task<string> HandleAuthenticationAsync(string username, string password)
        {
            if (await CanAuthenticateAsync(username, password))
            {
                var user = await GetUserAsync(username);
                if (user != null)
                {
                    var context = await CreateUserContextAsync(user);
                    if (context != null)
                    {
                        var identity = workContext.AuthenticationService.CreateIdentity<JwtContext>(context);
                        if (identity != null)
                        {
                            user.Logs.Add(new UserActivityLog
                            {
                                User = user,
                                Token = identity.Token,
                                LastLoginOn = DateTime.Now,
                            });

                            await logManager.Logger.InfoAsync($"Client ({context.Key}) is authenticated.");
                            return identity.Token;
                        }
                    }
                }
            }

            return default;
        }

        public async Task<PagedList<User>> GetUsersAsync(int pageIndex)
        {
            return await userRepository.FetchPagedAsync(x => x.UserLoginStatus == UserLoginStatus.Active, (o) => o.Asc(f => f.Name), 0, 10);
        }

        public Task<IEnumerable<UserLoginStatus>> GetUserStatus()
        {
            return userstatusRe.FetchAsync(x => x.Id < 5);
        }
    }
}
