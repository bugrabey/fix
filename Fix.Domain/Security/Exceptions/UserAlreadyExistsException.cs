﻿using Fix.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fix.Domain.Security.Exceptions
{
    public class UserAlreadyExistsException : OperationalException
    {
        public UserAlreadyExistsException(string message) : base(message)
        {

        }
    }
}
