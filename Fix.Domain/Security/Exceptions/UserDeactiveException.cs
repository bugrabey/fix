﻿using Fix.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fix.Domain.Security.Exceptions
{
    public class UserDeactiveException : OperationalException
    {
        public UserDeactiveException(string message) : base(message)
        {

        }
        public UserDeactiveException(string message, Exception innerException) : base(message, innerException)
        {

        }
    }
}
