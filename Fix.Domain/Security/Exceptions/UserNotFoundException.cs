﻿using Fix.Exceptions;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fix.Domain.Security.Exceptions
{
    public class UserNotFoundException : OperationalException
    {
        
        public UserNotFoundException(string message) : base(message)
        {

        }
        public UserNotFoundException(string message, Exception innerException) : base(message, innerException)
        {

        }

    }
}
