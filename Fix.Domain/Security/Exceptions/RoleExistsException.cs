﻿using Fix.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fix.Domain.Security.Exceptions
{
    public class RoleExistsException : OperationalException
    {
        public RoleExistsException(string message) : base(message)
        {

        }
        public RoleExistsException(string message, Exception innerException) : base(message, innerException)
        {

        }
    }
}
