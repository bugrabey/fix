﻿using Fix.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fix.Domain.Security.Exceptions
{
    public class PasswordMismatchException : OperationalException
    {
        public PasswordMismatchException(string message) : base(message)
        {

        }
    }
}
