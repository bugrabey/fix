﻿using Fix.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fix.Domain.Security.Exceptions
{
    public class UnKnowPermissionException : OperationalException
    {
        public UnKnowPermissionException(string message) : base(message)
        {

        }
    }
}
