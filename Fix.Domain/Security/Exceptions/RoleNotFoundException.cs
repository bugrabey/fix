﻿using Fix.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fix.Domain.Security.Exceptions
{
    public class RoleNotFoundException : OperationalException
    {
        public RoleNotFoundException(string message) : base(message)
        {

        }
        public RoleNotFoundException(string message, Exception innerException) : base(message, innerException)
        {

        }
    }
}
