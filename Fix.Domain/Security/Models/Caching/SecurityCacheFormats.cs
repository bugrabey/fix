﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Domain.Security.Models.Caching
{
    public static class SecurityCacheFormats
    {
        private const string prefix = "Security";
        public static string UserRoles(int id) => $"{prefix}_User({id})_Roles";
    }
}
