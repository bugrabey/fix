﻿using Fix.Data;
using Fix.Entity.Identity;
using Fix.Security;
using Fix.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fix.Domain.Security.Tasks
{
    public class DatabaseSeedTask : IStartupTask
    {
        private readonly ISecurityDomain securityDomain;
        private readonly ITransactionManager transactionManager;
        private readonly IPermissionProvider permissionProvider;

        public DatabaseSeedTask(
            ITransactionManager transactionManager,
            ISecurityDomain securityDomain,
            IPermissionProvider permissionProvider)
        {
            this.securityDomain = securityDomain ?? throw new ArgumentNullException(nameof(securityDomain));
            this.transactionManager = transactionManager ?? throw new ArgumentNullException(nameof(transactionManager));
            this.permissionProvider = permissionProvider ?? throw new ArgumentNullException(nameof(permissionProvider));
        }
        public bool CanStart => true;

        public void Execute()
        {
            var task =  securityDomain.Account.IsUserExistAsync("AllUser");
            task.Wait();
            if (!task.Result)
            {
                for (int i = 0; i < 1000; i++)
                {

                    var status = (i % 3 == 2) ? UserLoginStatus.Deleted : UserLoginStatus.Inactive;
                    var user = new User()
                    {
                        Username = "admin" + i.ToString(),
                        Password = "admin",
                        Name = "Buğra",
                        Surname = "İlbeyi",
                        UserLoginStatus = status
                    };
                    var role = new Role
                    {
                        Name = "System Administrator",
                        IsActive = true,
                        Permissions = permissionProvider.GetPermissions().Select(x => new RolePermission()
                        {
                            Permission = x
                        }).ToList(),
                    };
                    user.UserInRoles.Add(new UserInRole
                    {
                        Role = role
                    });

                    securityDomain.Account.CreateUserAsync(user).Wait();
                }
                transactionManager.Commit(System.Data.IsolationLevel.Serializable);
            }
        }
    }
}