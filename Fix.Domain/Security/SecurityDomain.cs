﻿using Fix.Domain.Security.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Domain.Security
{
    public class SecurityDomain: ISecurityDomain
    {
        public SecurityDomain(IAccountService accountService)
        {
            Account = accountService ?? throw new ArgumentNullException(nameof(accountService));
        }

        public IAccountService Account { get; }
    }
}
