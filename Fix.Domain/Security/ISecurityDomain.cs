﻿using Fix.Domain.Security.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Domain.Security
{
    public interface ISecurityDomain : IScoped
    {
        IAccountService Account { get; }
    }
}
