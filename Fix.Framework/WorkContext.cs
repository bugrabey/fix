﻿using System;
using System.Collections.Generic;
using System.Text;
using Fix.Environment;
using Fix.Environment.Dependency;
using Fix.Security;
using Fix.Security.OpenAuthentication;
using Microsoft.AspNetCore.Http;

namespace Fix
{
    public class WorkContext : IWorkContext
    {
        public WorkContext(
            IDependencySolver dependencySolver,
            IAuthenticationService authenticationService,
            IAuthorizer authorizer)
        {
            DependencySolver = dependencySolver ?? throw new ArgumentNullException(nameof(dependencySolver));
            AuthenticationService = authenticationService ?? throw new ArgumentNullException(nameof(authenticationService));
            Authorizer = authorizer ?? throw new ArgumentNullException(nameof(authorizer));
            CorrelationId = Guid.NewGuid();
        }

        public IDependencySolver DependencySolver { get; }
        public IAuthenticationService AuthenticationService { get; }
        public IAuthorizer Authorizer { get; }

        public Guid CorrelationId { get; }
    }
}
