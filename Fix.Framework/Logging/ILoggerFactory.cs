﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Logging
{
    public interface ILoggerFactory : ISingleton
    {
        ILogger GetLogger<T>() where T : ILogger;
        IEnumerable<ILogger> GetLoggers(LogLevel logLevels);
    }
}
