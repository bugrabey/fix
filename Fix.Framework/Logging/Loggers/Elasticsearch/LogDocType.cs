﻿using Nest;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Logging.Loggers.Elasticsearch
{
    [ElasticsearchType(Name = "log")]
    public class LogDocType : DocType
    {
        public override string Name { get => "log"; }
    }
}
