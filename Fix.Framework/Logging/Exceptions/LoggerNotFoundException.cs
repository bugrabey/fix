﻿using Fix.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Logging.Exceptions
{
    public class LoggerNotFoundException : FixException
    {
        public LoggerNotFoundException(string message) : base(message)
        {

        }
    }
}
