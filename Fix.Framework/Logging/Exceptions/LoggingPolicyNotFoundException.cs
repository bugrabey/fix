﻿using Fix.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Logging.Exceptions
{
    public class LoggingPolicyNotFoundException : FixException
    {
        public LoggingPolicyNotFoundException(string message) : base(message)
        {
        }

        public LoggingPolicyNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
