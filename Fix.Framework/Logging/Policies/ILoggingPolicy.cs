﻿using Fix.Logging.Iteration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Logging.Policies
{
    public interface ILoggingPolicy 
    {
        LogLevel Level { get; }
        ILogIterator Iterator { get; }
        IEnumerable<ILogger> Loggers { get; }
    }
}
