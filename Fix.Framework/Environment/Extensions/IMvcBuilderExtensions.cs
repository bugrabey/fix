﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Environment.Extensions
{
    public static class IMvcBuilderExtensions
    {
        public static IMvcBuilder UseFixConfiguration(this IServiceCollection services)
        {
            return services.AddMvc();
        }
    }
}
