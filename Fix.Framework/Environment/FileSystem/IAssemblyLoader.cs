﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace Fix.Environment.FileSystem
{
    public interface IAssemblyLoader
    {
        Assembly LoadIfNot(FileInfo fileInfo);
        //IEnumerable<Assembly> LoadAssemblyFiles(IEnumerable<FileInfo> files);

        //IEnumerable<Assembly> GetAssembliesMatchesFiles(IEnumerable<FileInfo> files);
    }
}
