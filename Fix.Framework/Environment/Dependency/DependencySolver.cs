﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Environment.Dependency
{
    public class DependencySolver : IDependencySolver
    {
        private readonly IHttpContextAccessor contextAccessor;

        public DependencySolver(IHttpContextAccessor contextAccessor)
        {
            this.contextAccessor = contextAccessor ?? throw new ArgumentNullException(nameof(contextAccessor));
        }
        public T Get<T>()
        {
            return (T)contextAccessor.HttpContext.RequestServices.GetService(typeof(T));
        }

        public object Get(Type type)
        {
            return contextAccessor.HttpContext.RequestServices.GetService(type);
        }
    }
}
