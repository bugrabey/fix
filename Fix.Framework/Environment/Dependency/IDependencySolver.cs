﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Environment.Dependency
{
    public interface IDependencySolver : IDependency
    {
        T Get<T>();
        object Get(Type type);
    }
}
