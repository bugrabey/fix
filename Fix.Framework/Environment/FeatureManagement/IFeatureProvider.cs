﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Fix.Environment.FeatureManagement
{
    public interface IFeatureProvider
    {
        IEnumerable<Feature> Get();
    }
    public class FeatureProvider : IFeatureProvider
    {
        private readonly IFeatureDirectoryService featureDirectoryService;

        public FeatureProvider(IFeatureDirectoryService featureDirectoryService)
        {
            this.featureDirectoryService = featureDirectoryService ?? throw new ArgumentNullException(nameof(featureDirectoryService));
        }
        public IEnumerable<Feature> Get()
        {
            yield return CreateAppFeature();
            foreach (var directory in featureDirectoryService.GetDirectories())
            {
                Console.WriteLine(directory.FullName);
                yield return CreateFeature(directory);
            }
        }

        private Feature CreateFeature(DirectoryInfo directoryInfo)
        {
            var files = directoryInfo.GetFiles("*.dll", SearchOption.AllDirectories).ToList();
            return new Feature
            {
                FriendlyName = directoryInfo.Name,
                Files = files
            };
        }

        private Feature CreateAppFeature()
        {
        
            var files = new List<FileInfo>
            {
               GetExecutingAppFileInfo()
            };
            return new Feature
            {
                FriendlyName = "AppStartUp",
                Files = files
            };
        }

        public static DirectoryInfo GetExecutingDirectory()
        {
            var location = new Uri(Assembly.GetEntryAssembly().GetName().CodeBase);
            return new FileInfo(location.AbsolutePath).Directory;
        }
        public static FileInfo GetExecutingAppFileInfo()
        {
            var location = new Uri(Assembly.GetEntryAssembly().GetName().CodeBase);
            return new FileInfo(location.AbsolutePath);
        }

    }
}
