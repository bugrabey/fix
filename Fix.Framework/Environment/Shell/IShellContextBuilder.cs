﻿using Autofac;
using Microsoft.Extensions.DependencyModel;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Fix.Environment.Shell
{
    public interface IDepedencyContextBuilder
    {
        DependencyContext Build(IEnumerable<Assembly> assemblies);
        DependencyContext Build(Assembly assembly);
    }
}
