﻿using Autofac;
using Fix.Environment.FeatureManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Environment.Shell
{
    public interface IWorkScopeBuilder
    {
        void Build(ContainerBuilder builder, FeatureContext context);
        
    }
}
