﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Environment.Aspects
{
    public class AspectPolicy
    {
        public Func<Type, bool> Predicate { get; set; }
        public IInterceptor Interceptor { get; set; }
    }
}
