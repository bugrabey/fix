﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace Fix.Environment.Aspects
{
    public sealed class AspectPolicyBuilder : IAspectPolicyBuilder
    {
        private static List<AspectPolicy> policiesHolder = new List<AspectPolicy>();

        public AspectPolicyBuilder Use(AspectPolicy policy)
        {
            policiesHolder.Add(policy);
            return this;
        }

        public AspectPolicyBuilder Use(IEnumerable<AspectPolicy> policies)
        {
            policiesHolder.AddRange(policies);
            return this;
        }
    }
}
