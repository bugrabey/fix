﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Environment.Aspects
{
    public interface IAspectPolicyBuilder
    {
        AspectPolicyBuilder Use(AspectPolicy policy);
        AspectPolicyBuilder Use(IEnumerable<AspectPolicy> policies);
    }
}
