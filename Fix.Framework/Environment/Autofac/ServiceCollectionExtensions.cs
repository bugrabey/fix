﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Fix.Environment.FeatureManagement;
using Fix.Environment.Shell;
using Fix.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Environment.Autofac
{
    public static class ServiceCollectionExtensions
    {

        public static IServiceProvider BuildProvider<T>(this IMvcBuilder mvcBuilder, Action<IServiceCollection> services = null) where T : AutofacServiceProvider
        {
            return ServiceProviderFactory.Create(mvcBuilder, services);
        }
    }
}
