﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Tasks.Shell
{
    public interface IShellTaskManager : IDependency
    {
        void Execute();
    }
}
