﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Tasks.Shell
{
    public interface IShellTask : IDependency
    {
        void Execute();
    }
}
