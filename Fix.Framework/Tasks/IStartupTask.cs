﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Tasks
{
    public interface IStartupTask : IDependency
    {
        bool CanStart { get; }
        void Execute();
    }
}
