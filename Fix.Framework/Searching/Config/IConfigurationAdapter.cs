﻿using Elasticsearch.Net;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Searching.Config
{
    public interface IConfigurationAdapter : IDependency
    {
        IConnectionPool ConnectionPoolType { get; set; }
        

    }
}
