﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Searching
{
    public interface IIndexInitializer : IScoped
    {
        T Init<T>() where T : BaseIndex;
    }
}
