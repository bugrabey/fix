﻿using Nest;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Searching
{
    public interface IDocType 
    {
        Guid Id { get; set; }
        string Name { get; }
    }
}
