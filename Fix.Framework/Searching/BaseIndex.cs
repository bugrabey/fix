﻿using Nest;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Searching
{
    public abstract class BaseIndex : IScoped
    {
        public ElasticClient Client { get; set; }
        public abstract string Name { get; }
        protected abstract void CreateIndex();
        protected abstract ElasticClient CreateClient();
        public void CreateIfNotExist()
        {
            Client = CreateClient();
            var response = Client.IndexExists(this.Name);
            if (!response.Exists)
            {
                CreateIndex();
            }
        }
    }
}
