﻿using Fix.Configuration;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Exceptions.Configuration
{
    public class ExceptionConfig: IConfigurationBase
    {
        public ExceptionConfig()
        {
            Policies = new List<PolicyConfig>();
        }
        public IList<PolicyConfig> Policies { get; set; }

        public Dictionary<string, string> HandlerTypes { get; set; }

        public bool IsValid()
        {
            return true;
        }

        public bool IsValid(out string message)
        {
            message = null;
            return true;
        }
    }
}
