﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Exceptions.Iteration
{
    public interface IIteratorFactory : IScoped
    {
        IExceptionIterator Create(string iterator);
    }
}
