﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Exceptions.Policy
{
    public interface IPolicyFactory : ISingleton
    {
        Policy Create(Type exceptionType);
    }
}
