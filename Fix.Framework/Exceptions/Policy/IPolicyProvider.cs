﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Exceptions.Policy
{
    public interface IPolicyProvider : ISingleton
    {
        Policy Get(Exception exception);
    }
}
