﻿using Fix.Logging;
using Fix.Logging.Loggers.FileLogger;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Fix.Exceptions.Handlers
{
    public class FileLoggerExceptionHandler : IExceptionHandler
    {
        private readonly TextFileLogger textFileLogger;

        public FileLoggerExceptionHandler(TextFileLogger textFileLogger)
        {
            this.textFileLogger = textFileLogger ?? throw new ArgumentNullException(nameof(textFileLogger));
        }


        public void Handle(Exception exception)
        {
            textFileLogger.Error(exception);
        }

        public async Task HandleAsync(Exception exception)
        {
            await textFileLogger.ErrorAsync(exception);
        }
    }
}
