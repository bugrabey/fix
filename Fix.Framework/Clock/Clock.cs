﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Clock
{
    public class Clock : IClock
    {
        public DateTime Timestamp()
        {
            return DateTime.Now;
        }
    }
}
