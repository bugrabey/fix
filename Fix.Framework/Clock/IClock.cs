﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Clock
{
    public interface IClock : IDependency
    {
        DateTime Timestamp();
    }
}
