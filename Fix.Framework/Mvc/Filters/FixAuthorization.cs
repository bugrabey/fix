﻿using Fix.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Fix.Security.OpenAuthentication;

namespace Fix.Mvc.Filters
{
    public class FixAuthorization : Attribute, IAuthorizationFilter
    {
        public string Permission { get; set; }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (!context.Filters.Any(item => item is IAllowAnonymousFilter))
            {
                var workContext = context.HttpContext.RequestServices.GetService<IWorkContext>();

                if (workContext.AuthenticationService.IsAuthenticated)
                {
                    if (!workContext.Authorizer.Authorize(Permission))
                    {
                        var builder = workContext.DependencySolver.Get<IActionResultBuilder>();
                        context.Result = builder.Build($"Forbidden access", "{Permission}", StatusCodes.Status403Forbidden);
                        return;
                    }
                    else return;
                }
                context.Result = new UnauthorizedResult();
            }
        }
    }
}
