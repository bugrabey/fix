﻿using Fix.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Mvc.Filters.Configuration
{
    public class FilterConfig : IConfigurationBase
    {
        public bool IsValid()
        {
            return true;
        }

        public bool IsValid(out string message)
        {
            message = string.Empty;
            return IsValid();
        }
    }
}
