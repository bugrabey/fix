﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fix.Mvc.Filters
{
    public class ValidationFilter : IActionFilter
    {
        private readonly IActionResultBuilder resultBuilder;
        public ValidationFilter(IActionResultBuilder resultBuilder)
        {
            this.resultBuilder = resultBuilder ?? throw new ArgumentNullException(nameof(resultBuilder));
        }
        public void OnActionExecuted(ActionExecutedContext context)
        {

        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.ModelState.IsValid)
            {
                if (!context.ActionArguments.Any(kv => kv.Value == null))
                {
                    return;
                }
            }
            context.Result = resultBuilder.Build(context.ModelState);
        }
    }
}
