﻿using Fix.Data;
using Fix.Logging;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fix.Mvc.Filters
{
    public class TransactionFilter : IResultFilter
    {
        private readonly ITransactionManager transactionManager;

        public TransactionFilter(ITransactionManager transactionManager)
        {
            this.transactionManager = transactionManager;
        }
        public void OnResultExecuted(ResultExecutedContext context)
        {
            transactionManager.Commit();
        }

        public void OnResultExecuting(ResultExecutingContext context)
        {

        }
    }
}
