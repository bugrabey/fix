﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Mvc.Filters.Extensions
{
    public static class FilterExtensions
    {
        public static IMvcBuilder AddFilters(this IMvcBuilder mvcBuilder)
        {
            return mvcBuilder.AddMvcOptions(x =>
              {
                  x.Filters.Add(typeof(FixExceptionFilter), int.MinValue);
                  x.Filters.Add(typeof(ValidationFilter), 3);
                  x.Filters.Add(typeof(TransactionFilter), int.MaxValue);
              });
        }

        public static IMvcBuilder AddFilters(this IMvcBuilder mvcBuilder, params Type[] args)
        {
            return mvcBuilder.AddMvcOptions(x =>
            {
                x.Filters.Add(typeof(FixExceptionFilter), int.MinValue);
                x.Filters.Add(typeof(ValidationFilter), 3);
                x.Filters.Add(typeof(TransactionFilter), int.MaxValue);
            });
        }
    }
}
