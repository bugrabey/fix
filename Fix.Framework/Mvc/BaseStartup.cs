﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Mvc
{
    public abstract class BaseStartup
    {
        protected IConfiguration Configuration { get; }
        public BaseStartup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfigurationSection GetSection(string sectionName)
        {
            return Configuration.GetSection(sectionName);
        }
    }
}
