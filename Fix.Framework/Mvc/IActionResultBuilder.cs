﻿using Fix.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fix.Mvc
{
    public interface IActionResultBuilder : IScoped
    {

        IActionResult Build(ModelStateDictionary dictionary);
        IActionResult Build(Exception exception);
        IActionResult Build(Exception exception, int statusCode);
        IActionResult Build(string error, string description, int statusCode);
    }

}

