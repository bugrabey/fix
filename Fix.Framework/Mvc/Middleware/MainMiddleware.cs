﻿using Fix.Caching;
using Fix.Security;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fix.Mvc.Middleware
{

    public class MainMiddleware
    {
        private readonly RequestDelegate next;
        private readonly IMiddlewareManager middlewareManager;

        public MainMiddleware(RequestDelegate next, IMiddlewareManager middlewareManager)
        {
            this.next = next;
            this.middlewareManager = middlewareManager ?? throw new ArgumentNullException(nameof(middlewareManager));
        }

        public async Task Invoke(HttpContext context)
        {
            if (await middlewareManager.OnRequest(context))
            {
                await next(context)
                    .ContinueWith(action => middlewareManager.OnResponse(context));
            }

            return;
        }
    }

    public static class MiddlewareExtensions
    {
        public static IApplicationBuilder UseFixMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<MainMiddleware>();
        }
    }
}
