﻿using Fix.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Fix.Data
{
    public interface IRepository<T> : IScoped where T : IEntity, new()
    {
        IQueryable<T> Table { get; }

        T FindBy(object id);
        void Add(T entity);
        Task AddAsync(T entity);
        void Delete(T entity);
        void Update(T entity);
        Task UpdateAsync(T entity);

        bool Any(Expression<Func<T, bool>> predicate);
        Task<bool> AnyAsync(Expression<Func<T, bool>> predicate);
        Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> predicate);

        IQueryable<T> Fetch(Expression<Func<T, bool>> predicate);
        IQueryable<T> Fetch(Expression<Func<T, bool>> predicate, Action<Orderable<T>> order);
        IQueryable<T> Fetch(Expression<Func<T, bool>> predicate, Action<Orderable<T>> order, int skip, int count);
        Task<IEnumerable<T>> FetchAsync(Expression<Func<T, bool>> predicate);
        Task<IEnumerable<T>> FetchAsync(Expression<Func<T, bool>> predicate, Action<Orderable<T>> order);
        Task<IEnumerable<T>> FetchAsync(Expression<Func<T, bool>> predicate, Action<Orderable<T>> order, int skip, int count);
        Task<PagedList<T>> FetchPagedAsync(Expression<Func<T, bool>> predicate, Action<Orderable<T>> order, int skip, int count);
    }
}
