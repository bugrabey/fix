﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Data
{
    public interface IEntityConfiguratorProvider : IDependency
    {
        IEnumerable<IEntityConfigurator<T>> GetConfigurators<T>() where T : DbContext;
    }
}
