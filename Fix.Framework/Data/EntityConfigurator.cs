﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Data
{
    public interface IEntityConfigurator<T> : IDependency where T : DbContext
    {
        void Configure(ModelBuilder builder);
    }
}
