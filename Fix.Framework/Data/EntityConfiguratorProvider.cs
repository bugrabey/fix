﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Fix.Data
{
    public class EntityConfiguratorProvider : IEntityConfiguratorProvider
    {
        private readonly IServiceProvider serviceProvider;

        public EntityConfiguratorProvider(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider ?? throw new ArgumentNullException(nameof(serviceProvider));
        }
        public IEnumerable<IEntityConfigurator<TDBContext>> GetConfigurators<TDBContext>() where TDBContext : DbContext
        {
            return serviceProvider.GetService(typeof(IEnumerable<IEntityConfigurator<TDBContext>>)) as IEnumerable<IEntityConfigurator<TDBContext>>;
        }
    }
}
