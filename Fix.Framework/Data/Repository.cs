﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Fix.Utility.Extensions;
using Fix.Utility;

namespace Fix.Data
{
    public class Repository<T> : IRepository<T> where T : class, IEntity, new()
    {
        protected readonly DbContext Context;
        protected virtual int PageCount => 10;
        protected virtual int TotalRowCount => Table.Count();
        public Repository(IDbContextLocator contextLocator)
        {
            if (contextLocator == null)
            {
                throw new ArgumentNullException(nameof(contextLocator));
            }

            Context = contextLocator.Current;
        }
        public IQueryable<T> Table
        {
            get
            {
                return Context.Set<T>();
            }
        }


        public T FindBy(object id)
        {
            return Context.Set<T>().Find(id);
        }
        public void Add(T entity)
        {
            Context.Set<T>().Add(entity);
        }
        public async Task AddAsync(T entity)
        {
            await Context.Set<T>().AddAsync(entity);
        }
        public void Delete(T entity)
        {
            Context.Set<T>().Attach(entity);
            Context.Set<T>().Remove(entity);
        }

        public void Update(T entity)
        {
            Context.Set<T>().Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
        }

        public async Task UpdateAsync(T entity)
        {
            Context.Set<T>().Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
        }
        public IQueryable<T> Fetch(Expression<Func<T, bool>> predicate)
        {
            return Table.Where(predicate);
        }

        public IQueryable<T> Fetch(Expression<Func<T, bool>> predicate, Action<Orderable<T>> order)
        {
            var orderable = new Orderable<T>(Fetch(predicate));
            order(orderable);
            return orderable.Queryable;
        }
        public IQueryable<T> Fetch(Expression<Func<T, bool>> predicate, Action<Orderable<T>> order, int skip, int count)
        {
            return Fetch(predicate, order).Skip(skip).Take(count);
        }

        public bool Any(Expression<Func<T, bool>> predicate)
        {
            return Table.Any(predicate);
        }

        public async Task<bool> AnyAsync(Expression<Func<T, bool>> predicate)
        {
            return await Table.AnyAsync(predicate);
        }

        public async Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> predicate)
        {
            return await Table.FirstOrDefaultAsync(predicate);
        }

        public async Task<IEnumerable<T>> FetchAsync(Expression<Func<T, bool>> predicate)
        {
            return await Fetch(predicate).ToListAsync();
        }

        public async Task<IEnumerable<T>> FetchAsync(Expression<Func<T, bool>> predicate, Action<Orderable<T>> order)
        {
            return await Fetch(predicate, order).ToListAsync();
        }

        public async Task<IEnumerable<T>> FetchAsync(Expression<Func<T, bool>> predicate, Action<Orderable<T>> order, int skip, int count)
        {
            return await Fetch(predicate, order, skip, count).ToListAsync();
        }
        public async Task<PagedList<T>> FetchPagedAsync(Expression<Func<T, bool>> predicate, Action<Orderable<T>> order, int skip, int count)
        {
            var page = await Fetch(predicate, order, skip, count).GroupBy(e => new { Total = Table.Count(predicate) }).FirstOrDefaultAsync();

            if (page != null)
            {
                int total = page.Key.Total;
                var items = page.Select(e => e).ToList();
                return new PagedList<T>(items, skip, count, total);
            }

            return default;
        }
    }


}
