﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;

namespace Fix.Data
{
    public sealed class TransactionManager : ITransactionManager
    {
        private readonly IDbContextLocator dbContextLocator;
        private readonly ITransactionStrategyManager strategyManager;

        public TransactionManager(IDbContextLocator dbContextLocator, ITransactionStrategyManager strategyManager)
        {
            this.dbContextLocator = dbContextLocator ?? throw new ArgumentNullException(nameof(dbContextLocator));
            this.strategyManager = strategyManager ?? throw new ArgumentNullException(nameof(strategyManager));
        }

        public DbContext Context
        {
            get
            {
                return dbContextLocator.Current;
            }
        }


        public void Commit()
        {
            Commit(IsolationLevel.ReadCommitted);
        }
        public void Commit(IsolationLevel level)
        {
            var changeset = GetChangedEntries();
            strategyManager.Apply(changeset);
            using (var transaction = Context.Database.BeginTransaction(level))
            {
                Context.SaveChanges();
                transaction.Commit();
            }
        }

        public void Rollback()
        {
            RejectChanges();
        }
        public async Task RollbackAsync()
        {
            await Task.Run(Rollback);
        }

        public void RejectChanges()
        {
            foreach (var entry in GetChangedEntries())
            {
                switch (entry.State)
                {
                    case EntityState.Modified:
                        entry.CurrentValues.SetValues(entry.OriginalValues);
                        entry.State = EntityState.Unchanged;
                        break;

                    case EntityState.Deleted:
                        entry.State = EntityState.Unchanged;
                        break;

                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                }
            }
        }

        public IEnumerable<EntityEntry> GetChangedEntries()
        {
            return Context.ChangeTracker.Entries().Where(x => x.State != EntityState.Unchanged);
        }
    }

}
