﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Fix.Data
{
    public interface ITransactionManager : IScoped
    {
        void Commit();
        void Commit(IsolationLevel level);
        void Rollback();
        Task RollbackAsync();
    }
}
