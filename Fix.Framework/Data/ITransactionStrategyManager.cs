﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Data
{
    public interface ITransactionStrategyManager : IScoped
    {
        void Apply(IEnumerable<EntityEntry> entries);
    }
}
