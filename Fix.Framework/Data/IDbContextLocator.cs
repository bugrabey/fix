﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Fix.Data
{
    public interface IDbContextLocator : IScoped
    {
        DbContext Current { get; }
    }

}
