﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Configuration
{
    public interface IConfigurationBase
    {
        bool IsValid();
        bool IsValid(out string message);
    }
}
