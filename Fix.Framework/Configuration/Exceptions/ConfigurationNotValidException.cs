﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Fix.Configuration.Exceptions
{
    public class ConfigurationNotValidException : Exception
    {
        public ConfigurationNotValidException()
        {
        }

        public ConfigurationNotValidException(string message) : base(message)
        {
        }

        public ConfigurationNotValidException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ConfigurationNotValidException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
