﻿using Fix.Environment;
using Fix.Environment.Dependency;
using Fix.Security;
using Fix.Security.OpenAuthentication;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fix
{
    public interface IWorkContext : IScoped
    {
        Guid CorrelationId { get; }
        IDependencySolver DependencySolver { get; }
        IAuthenticationService AuthenticationService { get; }
        IAuthorizer Authorizer { get; }

    }
}
