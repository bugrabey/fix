﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Host
{
    public static class WebHostBuilderExtensions
    {
        public static void Execute(this IWebHostBuilder webHostBuilder)
        {
            webHostBuilder.Build();
        }
    }
}
