﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fix
{
    public interface IDependency { }
    public interface IScoped : IDependency { }
    public interface ISingleton : IDependency { }
}
