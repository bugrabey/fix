﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Validation.Extensions
{
    public static class FluentValidationExtensions
    {
        public static IMvcBuilder UseFluentValidation(this IMvcBuilder mvcBuilder)
        {
            return mvcBuilder.Services.AddMvc(x =>
             {
                 x.ModelValidatorProviders.Clear();
                 x.ModelValidatorProviders.Add(new FixModelValidatorProvider());
             });
        }
    }
}
