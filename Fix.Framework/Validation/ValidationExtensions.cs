﻿using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Validation
{
    public static class ValidationExtensions
    {
        public static IMvcBuilder UseValidation<T>(this IMvcBuilder mvcBuilder) where T : IValidationProvider
        {
            mvcBuilder.Services.AddTransient(typeof(IValidationProvider), typeof(T));
            return mvcBuilder;
        }
    }
}
