﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fix.Security
{
    public interface IPermissionProvider : IScoped
    {
        IEnumerable<string> GetPermissions();
    }
}
