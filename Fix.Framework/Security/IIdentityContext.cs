﻿using Fix.Security.OpenAuthentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fix.Security
{
    public interface IIdentityContext 
    {
        string Key { get; set; }
    }
}
