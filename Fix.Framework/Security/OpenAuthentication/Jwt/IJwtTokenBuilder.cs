﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Security.OpenAuthentication.Jwt
{
    public interface IJwtTokenBuilder : IScoped
    {
        T Create<T>(string key) where T : IIdentityContext;
    }
}
