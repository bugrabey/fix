﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Security.OpenAuthentication
{
    public interface IClientContext 
    {
        string Key { get; }
    }

}
