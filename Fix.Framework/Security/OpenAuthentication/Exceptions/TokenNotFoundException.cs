﻿using Fix.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Security.OpenAuthentication.Exceptions
{
    public class TokenNotFoundException : FixException
    {
        public TokenNotFoundException(string message) : base(message)
        {

        }
    }
}
