﻿using Fix.Configuration.Exceptions;
using Fix.Security.OpenAuthentication;
using Fix.Security.OpenAuthentication.Exceptions;
using Fix.Security.OpenAuthentication.Jwt;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fix.Security.Extensions
{
    public static class AuthenticationExtensions
    {
        public static JwtConfig GetJwtConfig(this IConfiguration configuration, string name)
        {
            var section = configuration.GetSection(name);
            var config = new JwtConfig();
            section.Bind(config);
            return config;
        }

        public static IServiceCollection UseJwtAuthentication(this IServiceCollection services, JwtConfig config)
        {
            if (config == null || !config.IsValid())
            {
                throw new ConfigurationNotValidException(nameof(config));
            }

            return services
                .AddSingleton(typeof(JwtConfig), config)
                .AddScoped(typeof(IAuthenticationService), typeof(JwtAuthenticationService));

        }
    }
}
