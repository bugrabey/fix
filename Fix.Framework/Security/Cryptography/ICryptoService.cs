﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace Fix.Security.Cryptography
{
    public interface ICryptoService : IScoped
    {
        HashString Encrypt(string text, HashTypes hashType);
    }
}
