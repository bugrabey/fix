﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fix.Security
{
    public interface IAuthorizer : IScoped
    {
        bool Authorize(string permission);
       // Task<bool> AuthorizeAsync(string permission);
    }
}
