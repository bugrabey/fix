﻿using Fix.Logging;
using Fix.Mvc.Middleware;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fix.Hosting.App.Components
{
    public class LoggingComponent : IMiddlewareComponent
    {
        private readonly ILogManager logManager;

        public DateTime Begin { get; set; }
        public DateTime End { get; set; }
        public LoggingComponent(ILogManager logManager)
        {
            this.logManager = logManager ?? throw new ArgumentNullException(nameof(logManager));
        }
        public int SequenceNo => 1;

        public bool CanInvoke => true;

        public async Task<bool> OnRequest(HttpContext context)
        {
            Begin = DateTime.Now;
            var s = context.Connection.RemoteIpAddress;
            await logManager.Logger.InfoAsync(s);
            return true;
        }

        public async Task<bool> OnResponse(HttpContext context)
        {
            End = DateTime.Now;
            await logManager.Logger.InfoAsync($"request begin {Begin} - End {End}");
            return true;
        }
    }
}
