﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Fix.Hosting;
using Fix.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Autofac;
using Fix.Environment;
using Microsoft.Extensions.DependencyInjection;
using Fix.Host;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using System.Net;
using Fix.Exceptions.Configuration;
using Fix.Exceptions.Handlers;
using Newtonsoft.Json;
using Fix.Exceptions.Iteration;

namespace Fix.Services.Api
{

    public class Program
    {
        public static async Task Main(string[] args)
        {
            await BuildWebHost(args)
                    .InitFixHostAsync()
                    .ContinueWith((x) => x.Result.Run());
        }

        public static IWebHost BuildWebHost(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                //.UseKestrel(options =>
                //{
                //    options.Limits.MaxConcurrentConnections = 1000;
                //    options.Limits.MaxConcurrentUpgradedConnections = 100;
                //    options.Limits.MaxRequestBodySize = 10 * 1024;
                //})
                .Build();
        }
    }
}
