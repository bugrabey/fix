﻿using Fix.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Linq;

namespace Fix.Services.Api.Utility
{
    public class ResultBuilder : IActionResultBuilder
    {
        public IActionResult Build(object context)
        {
            throw new NotImplementedException();
        }

        public IActionResult Build(ModelStateDictionary modelState)
        {
            var states = modelState.ToDictionary(
                            kvp => kvp.Key,
                            kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()
                        );

            var obj = new { Error = "Model state is invalid.", ModelState = states };
            return GetResult(obj, StatusCodes.Status400BadRequest);
        }
        public IActionResult Build(Exception exception)
        {
            return Build(exception, StatusCodes.Status400BadRequest);
        }

        public IActionResult Build(Exception exception, int statusCode)
        {
            var obj = new { Error = exception.Message, Description = exception.StackTrace };
            return GetResult(obj, statusCode);
        }

        public IActionResult Build(string error, string description, int statusCode)
        {
            var obj = new { Error = error, Description = description };
            return GetResult(obj, statusCode);

        }

        public JsonResult GetResult(object obj, int statusCode = StatusCodes.Status400BadRequest)
        {
            return new JsonResult(obj)
            {
                StatusCode = statusCode
            };
        }




    }
}
