﻿using Autofac.Extensions.DependencyInjection;
using Fix.Data;
using Fix.Data.Extensions;
using Fix.Environment.Autofac;
using Fix.Environment.Extensions;
using Fix.Exceptions.Extensions;
using Fix.Logging.Extensions;
using Fix.Mvc;
using Fix.Mvc.Filters.Extensions;
using Fix.Mvc.Middleware;
using Fix.Security.Extensions;
using Fix.Validation.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json.Serialization;
using System;
using System.Net;

namespace Fix.Services.Api
{
    public class Startup : BaseStartup
    {
        public Startup(IConfiguration configuration) : base(configuration)
        {
            ServicePointManager.DefaultConnectionLimit = 5000;
        }
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            services.AddSwaggerDocument();
                //services.AddSwaggerDocument(options => { options.SwaggerDoc("v1", new Info { Title = "Fix API", Version = "v1" }); });

            return services
                .UseEntityFramework<FixDbContext>(Configuration.GetDatabaseConfig("DatabaseConfig"))
                .UseExceptions(Configuration.GetExceptionConfig("ExceptionConfig"))
                .UseLogging(Configuration.GetLoggingConfig("LoggingConfig"))
                .UseJwtAuthentication(Configuration.GetJwtConfig("JwtConfig"))
                .UseFixConfiguration()
                .AddNewtonsoftJson(x => x.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver())
                .AddFilters()
                .UseFluentValidation()
                .BuildProvider<AutofacServiceProvider>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseDeveloperExceptionPage();
            app.UseStaticFiles();
            app.UseCors("MyPolicy");
            app.UseFixMiddleware();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                        name: "default",
                        template: "{controller=Home}/{action=Index}/{id:int}");
            });

            app.UseSwagger();
            app.UseSwaggerUi3();
            //app.UseSwaggerUi3(c =>
            //{
            //    c.AdditionalSettings("/swagger/v1/swagger.json", "FIX API V1");
            //    c.RoutePrefix = string.Empty;
            //});
        }
    }
}
